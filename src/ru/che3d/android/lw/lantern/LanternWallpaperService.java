package ru.che3d.android.lw.lantern;

import java.util.Timer;
import java.util.TimerTask;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.IEntity.IEntityMatcher;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.QuadraticBezierMoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.ui.livewallpaper.BaseLiveWallpaperService;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.ease.EaseLinear;
import org.andengine.util.modifier.ease.EaseSineOut;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class LanternWallpaperService extends BaseLiveWallpaperService implements SharedPreferences.OnSharedPreferenceChangeListener {
	public static final String TAG = "Lantern lw";
	
//	private static final float BG_PARALLAX = 0f;
	private static final float PEOPLE_BACK_PARALLAX = 0.1f;
	private static final float PEOPLE_FRONT_PARALLAX = 0.3f;
	
	private static final float SCENE_WIDTH = 1400;
	private static final float SCENE_HEIGHT = 800;
	
	private BitmapTextureAtlas mTextureAtlas;
	private ITextureRegion mBackgroudTexture;
	private ITextureRegion mPeopleFrontTexture, mPeopleBackTexture;
	private ITiledTextureRegion[] mLanternsTexture;
	
	private Scene mScene;
	private Sprite mBackground, mPeopleBack, mPeopleFront;
	
	private Camera mCamera;
	
	private float mXOffset = 0;
	private boolean mIsPreview = true;
	
	private SharedPreferences mPrefs;

	@Override
	public EngineOptions onCreateEngineOptions() {
		final float width = getResources().getDisplayMetrics().widthPixels;
		final float height = getResources().getDisplayMetrics().heightPixels;
		final float idx = width / height;
		final float newWidth = SCENE_HEIGHT * idx;
		mCamera = new Camera(0f, 0f, newWidth, SCENE_HEIGHT);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mPrefs.registerOnSharedPreferenceChangeListener(this);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new RatioResolutionPolicy(newWidth, SCENE_HEIGHT), mCamera);
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		mTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 2048, 2048, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		
		BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "blank_2048x2048.png", 0, 0);
		mBackgroudTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "background.jpg", 5, 5);
		mPeopleFrontTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "people_front.png", 5, 810);
		mPeopleBackTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "people_back.png", 5, 1181);
		
		mLanternsTexture = new ITiledTextureRegion[]{
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_1_1.png", 1410, 5, 5, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_1_2.png", 1410, 135, 5, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_1_3.png", 1410, 265, 5, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_1_4.png", 1410, 395, 5, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_2_1.png", 1410, 525, 5, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "lantern_2_2.png", 1410, 650, 5, 1),
		};
		
		mTextureAtlas.load();

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		mScene = new Scene();
		final VertexBufferObjectManager vertexBufferObjectManager = getVertexBufferObjectManager();
		
		mScene.setBackground(new Background(0f, 0f, 0f));
		
		mBackground = new Sprite(0, 0, mBackgroudTexture, vertexBufferObjectManager);
		mPeopleBack = new Sprite(SCENE_WIDTH / 2 - mPeopleBackTexture.getWidth() / 2, SCENE_HEIGHT - mPeopleBackTexture.getHeight(), mPeopleBackTexture, vertexBufferObjectManager);
		mPeopleFront = new Sprite(SCENE_WIDTH / 2 - mPeopleFrontTexture.getWidth() / 2, SCENE_HEIGHT - mPeopleFrontTexture.getHeight(), mPeopleFrontTexture, vertexBufferObjectManager);
		final float pbx = (SCENE_WIDTH / 2 - mPeopleBackTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - mXOffset) * PEOPLE_BACK_PARALLAX;
		final float pfx = (SCENE_WIDTH / 2 - mPeopleFrontTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - mXOffset) * PEOPLE_FRONT_PARALLAX;
		mPeopleBack.setPosition(pbx , SCENE_HEIGHT - mPeopleBackTexture.getHeight());
		mPeopleFront.setPosition(pfx, SCENE_HEIGHT - mPeopleFrontTexture.getHeight());
		
		mScene.attachChild(mBackground);
		final int lanternsCount = Integer.valueOf(mPrefs.getString("lanterns_count", "50"));
		final String lanternsSpeed = mPrefs.getString("lanterns_speed", "slow");
		createRandomLanterns(mScene, lanternsCount, lanternsSpeed);
	
		mScene.attachChild(mPeopleBack);
		mScene.attachChild(mPeopleFront);
		
		mPeopleBack.setZIndex(2);
		mPeopleFront.setZIndex(3);
		mBackground.setZIndex(0);

		pOnCreateSceneCallback.onCreateSceneFinished(mScene);

	}
	
	private void createRandomLanterns(final Scene scene, final int count, final String speed) {
		Timer timer = new Timer();
		
		for(int i = 0; i < 5; i++) {
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					createBunchOfLanterns(scene, count / 5, speed);
					mScene.sortChildren();
				}
			}, 1000);
		}
	}
	
	private void createBunchOfLanterns(final Scene scene, final int count, final String speed) {
		final float y = SCENE_HEIGHT - 200;
		float speedMin = 0f;
		float speedMax = 0f;
		if(speed.equals("fast")){
			speedMin = 10f;
			speedMax = 30f;
		} else if(speed.equals("normal")) {
			speedMin = 15f;
			speedMax = 45f;
		} else {
			speedMin = 20f;
			speedMax = 60f;
		}
		for(int i = 0; i < count; i++) {
			final AnimatedSprite lantern = createLantern(MathUtils.random(200f, 1200f), y, MathUtils.random(speedMin, speedMax), MathUtils.random(0f, 10f));
			scene.attachChild(lantern);
			lantern.setZIndex(1);
		}
	}
	
	private ITiledTextureRegion getRandromLanternTexture() {
		final int n = MathUtils.random(0, mLanternsTexture.length - 1);
		return mLanternsTexture[n];
	}
	
	private AnimatedSprite createLantern(final float x, final float y, final float speed, final float delay) {
		
		final AnimatedSprite lantern = new AnimatedSprite(x, y, getRandromLanternTexture(), getVertexBufferObjectManager());
		lantern.animate(100);
		lantern.setAlpha(0);
		lantern.registerEntityModifier(
				new LoopEntityModifier(
						new SequenceEntityModifier(
								new DelayModifier(delay),
								new ParallelEntityModifier(
										new AlphaModifier(speed * 0.10f, 0, 1),
										new MoveModifier(speed * 0.30f, x + 60, x, y, SCENE_HEIGHT - 280),
										new ScaleModifier(speed * 0.30f, 1, 1)
								),
								new ParallelEntityModifier(
										new QuadraticBezierMoveModifier(speed * 1.0f, x, SCENE_HEIGHT -280, x - 70, SCENE_HEIGHT - 500, x - 200, SCENE_HEIGHT - 650, EaseLinear.getInstance()),
										new ScaleModifier(speed * 1.0f, 1, 0.3f)
								),
								new ParallelEntityModifier(
										new QuadraticBezierMoveModifier(speed * 1.3f, x - 200, SCENE_HEIGHT - 650, x - 280, SCENE_HEIGHT - 740, x - 550, -lantern.getHeight(), EaseSineOut.getInstance()),
										new ScaleModifier(speed * 1.3f, 0.3f, 0f)
								)
						)
				)
		);		
		return lantern;
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		pOnPopulateSceneCallback.onPopulateSceneFinished();

	}
	
	@Override
	public void onSurfaceChanged(int pWidth, int pHeight) {
		final float idx = (float)pWidth / pHeight;
		final float width = SCENE_HEIGHT * idx;
		mCamera.set(0, 0, width, SCENE_HEIGHT);
		if(mIsPreview) {
			mCamera.setCenter(SCENE_WIDTH / 2, mCamera.getCenterY());
		}
		else {
			final float cameraOffset = SCENE_WIDTH * mXOffset - mCamera.getWidth() * mXOffset + mCamera.getWidth() / 2;
			mCamera.setCenter(cameraOffset, mCamera.getCenterY());
			if(mScene != null) {
				final float pbx = (SCENE_WIDTH / 2 - mPeopleBackTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - mXOffset) * PEOPLE_BACK_PARALLAX;
				final float pfx = (SCENE_WIDTH / 2 - mPeopleFrontTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - mXOffset) * PEOPLE_FRONT_PARALLAX;
				mPeopleBack.setPosition(pbx , SCENE_HEIGHT - mPeopleBackTexture.getHeight());
				mPeopleFront.setPosition(pfx, SCENE_HEIGHT - mPeopleFrontTexture.getHeight());
			}
		}
		
		super.onSurfaceChanged(pWidth, pHeight);
	}
	
	@Override
	protected void onOffsetsChanged(float pXOffset, float pYOffset,
			float pXOffsetStep, float pYOffsetStep, int pXPixelOffset,
			int pYPixelOffset) {
		if(pXOffsetStep != 0f) {
			mIsPreview = false;
		}
		else {
			mIsPreview = true;
		}
		
		mXOffset = pXOffset;
		
		final float cameraOffset = SCENE_WIDTH * pXOffset - mCamera.getWidth() * pXOffset;
		mCamera.setCenter(cameraOffset + mCamera.getWidth() / 2, mCamera.getCenterY());
		
		if(mScene != null) {
			final float pbx = (SCENE_WIDTH / 2 - mPeopleBackTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - pXOffset) * PEOPLE_BACK_PARALLAX;
			final float pfx = (SCENE_WIDTH / 2 - mPeopleFrontTexture.getWidth() / 2) + SCENE_WIDTH * (0.5f - pXOffset) * PEOPLE_FRONT_PARALLAX;
			if(mPeopleBack != null) {
				mPeopleBack.setPosition(pbx , SCENE_HEIGHT - mPeopleBackTexture.getHeight());
			}
			if(mPeopleFront != null) {
				mPeopleFront.setPosition(pfx, SCENE_HEIGHT - mPeopleFrontTexture.getHeight());
			}
		}
		
		super.onOffsetsChanged(pXOffset, pYOffset, pXOffsetStep, pYOffsetStep,
				pXPixelOffset, pYPixelOffset);
	}
	
	@Override
	protected synchronized void onResume() {
		super.onResume();
		if(isGamePaused()) {
			onResumeGame();
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if(key.equals("lanterns_count") || key.equals("lanterns_speed")) {
			final int lanternsCount = Integer.valueOf(sharedPreferences.getString("lanterns_count", "50"));
			final String lanternsSpeed = mPrefs.getString("lanterns_speed", "slow");
			mScene.detachChildren(new IEntityMatcher() {
				
				@Override
				public boolean matches(IEntity pEntity) {
					if(pEntity.getZIndex() == 1) {
						return true;
					}
					else {
						return false;
					}
				}
			});
			createRandomLanterns(mScene, lanternsCount, lanternsSpeed);
			mScene.sortChildren();
		}
		
	}

}
